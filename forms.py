from flask_wtf import FlaskForm
from wtforms import TextField, SelectField, PasswordField
from wtforms.validators import DataRequired, EqualTo, Length, Email
from wtforms.fields.html5 import EmailField

class MandrillRegistrationForm(FlaskForm):
    mandrill_key = TextField(
        'Mandrill API Key', validators=[DataRequired(), Length(min=6, max=22)]
    )
    mandrill_field_name = TextField(
        'Name', validators=[DataRequired(), Length(min=3, max=25)]
    )
    mandrill_field_email = EmailField('Email address', validators=[DataRequired(), Email()])

class MandrillWelcomeBackForm(FlaskForm):
    mandrill_key = TextField(
        'Mandrill API Key', validators=[DataRequired(), Length(min=6, max=22)]
    )
    mandrill_field_name = TextField(
        'Name', validators=[DataRequired(), Length(min=3, max=25)]
    )
    mandrill_field_gift_code = TextField(
        'Gift code', validators=[DataRequired(), Length(min=6, max=25)]
    )
    mandrill_field_gift_code_value = TextField(
        'Gift code value', validators=[DataRequired(), Length(min=6, max=25)]
    )
    mandrill_field_email = EmailField('Email address', validators=[DataRequired(), Email()])
