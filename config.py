import os

DEBUG = True
SECRET_KEY = '"s3cr3t_k3y'
HOST = 'localhost'
PORT = int(os.environ.get('PORT', 5000))