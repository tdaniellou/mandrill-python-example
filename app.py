#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import Flask, render_template, request, flash
from forms import *

import mandrill

#----------------------------------------------------------------------------#
# Application
#----------------------------------------------------------------------------#

# Create the app
app = Flask(__name__)

# Set the configuration
app.config.from_object('config')

#----------------------------------------------------------------------------#
# Controllers
#----------------------------------------------------------------------------#

@app.route('/', methods=['GET', 'POST'])
def index():
	form = MandrillRegistrationForm(request.form)
	error = None

	if request.method == 'POST':
		if form.validate():
			# Form values
			mandrill_key = form.mandrill_key.data
			name = form.mandrill_field_name.data
			email = form.mandrill_field_email.data

			# Here we set the merge vars
			merge_vars = { 'cleo': name }

			# Call the API
			results = send_mail(mandrill_key, 'registration', [email], merge_vars)

			for result in results:
				if result['status'] == 'rejected':
					flash('An error has occured: ' + result['reject_reason'], 'danger')
				else:
					flash('The email was sent!', 'success')
		else:
			flash('The form is invalid', 'danger')

	return render_template('pages/registration.html', form=form, error=error)

@app.route('/welcome-back/', methods=['GET', 'POST'])
def welcome_back():
	form = MandrillWelcomeBackForm(request.form)
	error = None

	if request.method == 'POST':
		if form.validate():
			# Form values
			mandrill_key = form.mandrill_key.data
			name = form.mandrill_field_name.data
			email = form.mandrill_field_email.data
			gift_code = form.mandrill_field_gift_code.data
			gift_code_value = form.mandrill_field_gift_code_value.data

			# Merge vars
			merge_vars = {
				'fname': name,
				'gift': gift_code,
				'value': gift_code_value
			}

			# Call the API
			results = send_mail(mandrill_key, 'welcome-back', [email], merge_vars)

			for result in results:
				if result['status'] == 'rejected':
					flash('An error has occured: ' + result['reject_reason'], 'danger')
				else:
					flash('The email was sent!', 'success')
		else:
			flash('Invalid form', 'danger')

	return render_template('pages/welcome-back.html', form=form, error=error)

# Send mail
def send_mail(api_key, template_name, recipients, merge_vars):
	try:
		# Create the Mandrill Client, instead of passing the API_KEY as a parameter, we can set it in the config file
		mandrill_client = mandrill.Mandrill(api_key)

		# Message parameters
		# Here, we can use 2 type of merge vars: `global_merge_vars` or `merge_vars`
		# 	global_merge_vars: Will have the same variables in every emails
		# 	merge_vars: Will have one specific variable per recipient
		# In this example, we use merge_vars
		message = {
			'from_email': 'gigi@photageapp.com',	# `from_name` and `from_email` need to be set
			'from_name': 'Photage',					# otherwise mandrill will reject the email
			'to': [],
			'merge_vars': []
		}

		# For each recipient we create add an object in the merge_vars array
		#
		# Example:
		# "merge_vars": [
		#     {
		#         "rcpt": "recipient@example.com",
		#         "vars": [
		#             {
		#                 "name": "FNAME",
		#                 "content": "Jane"
		#             },
		#              {
		#                 "name": "ORDERID",
		#                 "content": "123456"
		#             }
		#         ]
		#     }
		# ]
		#
		# Some tags are retrieved automatically by Mandrill
		# See: https://mandrill.zendesk.com/hc/en-us/articles/205582787-MailChimp-Merge-Tags-Supported-in-Mandrill
		for recipient in recipients:
			message['to'].append({'email': recipient})

			# Parse the merge vars
			vars = []
			for key, value in merge_vars.iteritems():
				vars.append(
					{'name': key, 'content': value}
				)

			message['merge_vars'].append({ 'rcpt': recipient, 'vars': vars})

		# Call Mandrill API
		return mandrill_client.messages.send_template(template_name, [], message)

	except mandrill.Error, e:
		# Mandrill errors are thrown as exceptions
		print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
		raise

if __name__ == '__main__':
	app.run();
