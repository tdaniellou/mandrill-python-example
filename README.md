# Mandrill API
This is a small Flask app to test the Mandrill integration for python.

### How to install:
  * cd to this folder
  * create a 'virtual environment' by running `virtualenv env`
  * activate the environment `. env/bin/activate`
  * run `pip install requirements.txt` (to install the dependencies)

### Developer mode
  * Run `python app.py`
  * go on http://localhost:5000/ to access the website